# pbkdf2

`pbkdf2` is a convience library wrapping
[go.crypto/pbkdf2](http://go.pkgdoc.org/code.google.com/p/go.crypto/pbkdf2).
By default, it uses the HMAC-SHA1 scheme with 16,384 iterations and
yielding a 32-byte hash suitable for use as an AES-256 key and 16-byte salts,
double the recommended minimum of 8 bytes; this is compliant with the
[RFC 2898](http://tools.ietf.org/html/rfc2898) /
[PKCS #5 v2.0](http://www.rsa.com/rsalabs/node.asp?id=2127) standard. It is
ISC licensed and may be used for any commercial or non-commercial applications.
It also contains a test suite to validate the functionality provided, which
also provides examples for using the library.

## Godoc

The library is documented using [godoc](http://golang.org/cmd/godoc/). To
view the documentation, you can either run

```
$ godoc .
```

from inside the projects directory, or, if installed to `${GOPATH}`, can be
viewed with 

```
$ godoc github.com/gokyle/pbkdf2
```
.

The `godoc` program can also run an HTTP server locally, so that you may view
the docuentation in your browser; see `godoc godoc`.


## Installation / Getting the source

The source may be fetched from github in a number of ways:

```
$ git clone https://github.com/gokyle/pbkdf2
$ git clone git://github.com/gokyle/pbkdf2
```

or using the built-in Go tools (which have the advantage of ensuring
the code is downloaded to `${GOPATH}`:

```
go get github.com/gokyle/pbkdf2
go install github.com/gokyle/pbkdf2
```

Alternatively, including the library in your project's import directives and
running `go get` on the project will fetch the source.

The tests may be run with

```
go test github.com/gokyle/pbkdf2
```
.

For more verbose test output (i.e. to view the tests as they run, pass the
`-v` flag to `go test`.

## Example

```
package auth 

import (
        "fmt"
        "github.com/gokyle/pbkdf2"
)

// Assumes another source file contains the actual database lookup code.
// The function GetUserHashFromDatabase should return the stored hash and
// salt for the given username.
func LoadUserHash(username string) *pbkdf2 {
        hash, salt := GetUserHashFromDatabase(username)
        return &pbkdf2.PasswordHash(hash, salt)
}

func Authenticate(username, password string) bool {
        fmt.Println("[+] authenticating ", username)
        return pbkdf2.MatchPassword(password, LoadUserHash(username))
}
```
