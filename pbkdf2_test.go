package pbkdf2

import (
	"bytes"
	"fmt"
	"testing"
	"time"
)

const testPass = "hello, world"

var refSalt = []byte{0xb0, 0x0d, 0xd8, 0xe1, 0x57, 0x1f, 0x5a, 0xd5, 0x40,
	0xc5, 0x04, 0xf1, 0xcd, 0x13, 0x50, 0xf9}
var refHash = []byte{0x48, 0x3e, 0x57, 0x20, 0x24, 0xe6, 0x86, 0xd1, 0xca,
	0x9a, 0x35, 0xed, 0x84, 0xae, 0x8c, 0x83, 0x22, 0xa1,
	0xb0, 0xcd, 0x0b, 0x81, 0x33, 0xdd, 0x14, 0x52, 0x62,
	0xf2, 0xfc, 0xf6, 0xc7, 0x96}
var refPH = &PasswordHash{refHash, refSalt}

func TestHashPasswordWithSalt(t *testing.T) {
	fmt.Printf("[+] testing HashPasswordWithSalt: ")
	ph := HashPasswordWithSalt(testPass, refSalt)
	if !bytes.Equal(ph.Hash, refHash) {
		fmt.Println("failed")
		fmt.Println("[!] hashes do not match")
		t.FailNow()
	} else if !bytes.Equal(ph.Salt, refSalt) {
		fmt.Println("failed")
		fmt.Println("[!] salts do not match")
		t.FailNow()
	}
	fmt.Println("ok")
}

func TestMatchPassword(t *testing.T) {
	fmt.Printf("[+] testing MatchPassword (and HashPassword): ")
	ph := HashPassword(testPass)
	if !MatchPassword(testPass, ph) {
		fmt.Println("failed")
		fmt.Println("[!] password match failed when it should have passed")
		t.FailNow()
	}
	fmt.Println("ok")
}

func TestEnsureFails(t *testing.T) {
	fmt.Printf("[+] ensuring authentication fails for wrong password: ")
	if MatchPassword("hello world", refPH) {
		fmt.Println("failed")
		fmt.Println("[!] authentication should not have succeeded!")
		t.FailNow()
	}
	fmt.Println("ok")
}

func TestEmptyPassFails(t *testing.T) {
	fmt.Printf("[+] ensuring empty password fails: ")
	if MatchPassword("", refPH) {
		fmt.Println("failed")
		fmt.Println("[!] authentication should not have succeeded!")
		t.FailNow()
	}
	fmt.Println("ok")
}

func BenchmarkHashPassword(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HashPassword(testPass)
	}
}

func TestOneHashTime(t *testing.T) {
	fmt.Println("[+] testing time for one password check")
	start := time.Now()
	MatchPassword("goodbye world", refPH)
	stop := time.Now()
	spent := stop.Sub(start)
	fmt.Printf("[+] time spent: %s\n", spent.String())
}
