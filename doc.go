/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
Package pbkdf2 implements a wrapper around the go.crypto pbkdf2 package to
make it easier to use right away.

To create a new password hash, call HashPassword; this takes a string and
returns a PasswordHash whose fields contain the hash and salt. These values
can be safely stored, i.e. in a database for web applications. To check a
password later, compare a PasswordHash containing the stored hash and salt
with the suspect password using MatchPassword. For example:

        func checkPassForUser(password, username string) bool {
                hash, salt := GetUserHash(username)
                return MatchPassword(password, &pbkdf.PasswordHash{hash, salt})
        }

By default, this package uses the HMAC-SHA1 scheme with 16,384 iterations and
yielding a 32-byte hash suitable for use as an AES-256 key and 16-byte salts,
double the recommended minimum of 8 bytes.

For more information, see the documentation for go.crypto/pbkdf2 or RFC 2898
(PKCS #5 v2.0).
*/
package pbkdf2
