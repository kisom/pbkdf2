// Command line tool to write a reference salt and hash to a file
package main

import (
        "bufio"
	"crypto/sha256"
        "fmt"
        "github.com/gokyle/pbkdf2"
        "io/ioutil"
        "os"
        "strings"
)


func init() {
        pbkdf2.HashFunc = sha256.New
        pbkdf2.SaltSize = 32
}


// Write hash to file
func WriteHash(password string, filename string) (err error) {
        ph := pbkdf2.HashPassword(password)

        phOut := make([]byte, 0)
        phOut = append(phOut, ph.Salt...)
        phOut = append(phOut, ph.Hash...)

        err = ioutil.WriteFile(filename, phOut, 0600)
        return
}


func readPrompt(prompt string) (input string, err error) {
	fmt.Printf(prompt)
	rd := bufio.NewReader(os.Stdin)
	line, err := rd.ReadString('\n')
	if err != nil {
		return
	}
	input = strings.TrimSpace(line)
	return
}


func main() {
        if len(os.Args) == 0 {
                fmt.Printf("[!] please supply an output file.\n")
                os.Exit(1)
        }

        passphrase, err := readPrompt("passphrase: ")
        if err != nil {
                fmt.Printf("[!] error reading passphrase: %s\n",
                           err.Error())
                os.Exit(1)
        }

        for _, fname := range os.Args[1:] {
                if err = WriteHash(passphrase, fname); err != nil {
                        fmt.Printf("[!] error writing to %s\n", err.Error())
                }
        }
}
