genrefhash is a command line tool for generating a PasswordHash and writing
it to a file. This could be used, for example, for generating test vectors.
